let moveX, moveY;
let x, y;
let w, z;
let score;
let viewWidth, viewHeight;
let paddleSize;
let version;
let isGameOver;

function setup() {
  viewWidth = window.innerWidth || 1280;
  viewHeight = window.innerHeight || 720;

  createCanvas(viewWidth, viewHeight);
  background(0);

  x = width/2;
  y = height/2;

  paddleSize = 200;

  moveX = random(-5, -8);
  moveY = random(-5, -8);

  w = 25;
  z = 0;
  score = 0;

  textFont("Trueno")
  textStyle(BOLD);

  version = "0.0.3 - by y3n";
  isGameOver = false;
}


function draw() {
  clean();
  move();
  bounce();
  build();
}


function clean() {
  background(int(score*10), 0, 0);
}

function build() {
  smooth();

  stroke(0);
  strokeWeight(2);

  fill(255);
  rect(w, z, 15, paddleSize, 15);
  fill(255);
  ellipse(x, y, 20, 20);
  fill(255);

  strokeWeight(4);
  textAlign(CENTER);
  textSize(48);
  text(score, width/2, 100);

  textAlign(CENTER);
  textSize(24);
  text(version, width/2, height-15);
}

function move() {
  x = x + moveX;
  y = y + moveY;

  z = mouseY-paddleSize/2;
  if(z > height-paddleSize) {
    z = height-paddleSize
  }
  if(z < 0) {
    z = 0
  }
}

function bounce() {
  // si on est trop à droite et que le déplacement horizontal est positif
  if(x > width-10 && moveX > 0) {
    // inverser la valeur
    moveX = -moveX;
  }

  // si on est trop bas et le déplacement vertical est positif
  if(y > height-10 && moveY > 0) {
    // rendre négative la valeur
    moveY = -moveY;
  }

  // si on est trop haut et le déplacement vertical est negatif
  if(y < 10 && moveY < 10) {
    // rendre positive cette valeur
    moveY = abs(moveY);
  }

  // on verifie d'abord si on est devant la plateforme
  // si on est pas derrière
  // si on est pas au dessus
  // si on est pas en dessous
  if(x < w+25 && x > w+20 && y+10 > z && y < z+paddleSize+10) {
    score++;
    //moveX = random(moveX+score/4, moveX-score/4)
    //moveY = random(moveY-score, moveY-random(score, score))
    if(paddleSize > 80) {
      moveX = moveX-random(1, score);
      paddleSize = paddleSize-3;
    }
    moveX = -moveX; // inverser la valeur
  }

  if(x < -10) {
    noLoop();
    gameOver();
  }
}

function gameOver() {
  isGameOver = true;
  textSize(72);
  fill(255);
  textAlign(CENTER);
  text("GAME OVER", width/2, height/2-120);
  textSize(40);
  text("Click to try again", width/2, height/2+40);
  //console.log(`GAME OVER - Score: ${score}`);
}

function mouseClicked() {
  if(isGameOver) {
    setup();
    loop();
  }
}
