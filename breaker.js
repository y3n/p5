p5.disableFriendlyErrors = true;

let moveX, moveY;
let x, y;
let w, z;
let score;
let viewWidth, viewHeight;
let paddleSize;
let version;
let isGameOver;
let isGameWon;

function setup() {
  viewWidth = 704;
  viewHeight = 480;

  bMap = breakerMap.slice();

  createCanvas(viewWidth, viewHeight);
  background(0);

  x = width/2-200;
  y = height/2;

  paddleSize = 200;

  moveX = random(-2, -5);
  moveY = random(-2, -5);

  w = 25;
  z = 0;
  score = bMap.length;

  textFont("Trueno")
  textStyle(BOLD);

  version = "0.0.1 - by y3n";
  isGameWon = false;
  isGameOver = false;
}


function draw() {
  clean();
  blocks();
  bounce(width, height);
  move();
  build();
}


function clean() {
  background(int(breakerMap.length-score)+(255-breakerMap.length), 0, 0);
}

function blocks() {
  for(i = 0; i < bMap.length; i++) {
    var block = bMap[i];

    if(x+10 > block.x && x-10 < block.x+15 && y+10 > block.y && y-10 < block.y+60) {
      //console.log(`hit ${i}`)
      bMap.splice(i, 1);
      score--;
      if(score == 0) isGameWon = true;

      // si on est trop à droite et que le déplacement horizontal est positif
      if(x+30 > block.x && moveX > 0) {
        //console.log(`si on est trop à droite et que le déplacement horizontal est positif`)
        moveX = -moveX;
      } else if(x < block.x+30 && moveX < 0) {
        //console.log(`si on est trop à gauche et le déplacement vertical est negatif`)
        moveX = -moveX;
      }

      // si on est trop bas et le déplacement vertical est positif
      /*if(y > block.y-10 && moveY > 0) {
        console.log(`si on est trop bas et le déplacement vertical est positif`)
        moveY = -moveY;
      }

      // si on est trop haut et le déplacement vertical est negatif
      if(y < block.y+60+10 && moveY < 10) {
        console.log(`si on est trop haut et le déplacement vertical est negatif`)
        moveY = abs(moveY);
      }*/

      // si on est trop à gauche et le déplacement vertical est negatif

      if(paddleSize > 80) {
        increaseDifficulty();
      }
    }
  }
}

function build() {
  smooth();

  // stroke
  stroke(0);
  strokeWeight(2);

  // blocks
  for(i = 0; i < bMap.length; i++) {
    fill(255);
    rect(bMap[i].x, bMap[i].y, 15, 60, 15);
  }

  // paddle
  fill(255);
  rect(w, z, 15, paddleSize, 15);

  // ball
  fill(255);
  ellipse(x, y, 20, 20);

  // score
  fill(255);
  strokeWeight(4);
  textAlign(CENTER);
  textSize(48);
  text(score, width/2, 100);

  // version
  textAlign(CENTER);
  textSize(24);
  text(version, width/2, height-15);

  // fps
  var fps = frameRate();
  fill(255);
  stroke(0);
  text("FPS: " + fps.toFixed(2), 70, height - 5);

  if(isGameOver) gameOver();
  else if(isGameWon) gameWon();
}

function move() {
  x = x + moveX;
  y = y + moveY;

  z = mouseY-paddleSize/2;
  if(z > height-paddleSize) {
    z = height-paddleSize
  }
  if(z < 0) {
    z = 0
  }
}

function bounce(relWidth, relHeight) {
  // si on est trop à droite et que le déplacement horizontal est positif
  if(x > width-10 && moveX > 0) {
    // inverser la valeur
    moveX = -moveX;
  }

  // si on est trop bas et le déplacement vertical est positif
  if(y > height-10 && moveY > 0) {
    // rendre négative la valeur
    moveY = -moveY;
  }

  // si on est trop haut et le déplacement vertical est negatif
  if(y < 10 && moveY < 10) {
    // rendre positive cette valeur
    moveY = abs(moveY);
  }

  // on verifie d'abord si on est devant la plateforme
  // si on est pas derrière
  // si on est pas au dessus
  // si on est pas en dessous
  if(x < w+30 && x+10 > w && y+10 > z && y < z+paddleSize+10) {
    moveX = -moveX; // inverser la valeur
  }

  if(x < -10) {
    isGameOver = true;
  }
}

function increaseDifficulty() {
  moveX = moveX-0.5;
  paddleSize = paddleSize-3;
}

function gameWon() {
  textSize(72);
  fill(255);
  textAlign(CENTER);
  text("YOU WON!", width/2, height/2-120);
  textSize(40);
  text("Click to play again", width/2, height/2+40);
  noLoop();
}

function gameOver() {
  textSize(72);
  fill(255);
  textAlign(CENTER);
  text("GAME OVER", width/2, height/2-120);
  textSize(40);
  text("Click to try again", width/2, height/2+40);
  noLoop();
  //console.log(`GAME OVER - Score: ${score}`);
}

function mouseClicked() {
  if(isGameOver || isGameWon) {
    setup();
    loop();
  }
}
