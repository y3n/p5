var circleX = 50;
var circleY = 360;

var deplacementX = 5;
var deplacementY = 6;

var rectangleL = 130;

var score = 0;


function setup() {
  createCanvas(1080, 720);
}

function draw(){
  background(50);
  textFont("SegoeUI", 48);
  // Score
  text(score, 540, 90);

  // Balle
  fill(255);
  ellipse(circleX, circleY, 25, 25);

  // Raquette
  stroke(255);
  rect(5, mouseY, 25, rectangleL);


  // Déplacement et rebondissement
  circleX = circleX + deplacementX;
  circleY = circleY + deplacementY;

  if ((circleY >= mouseY) && (circleY <= mouseY + 130) && (circleX <= 30) && (circleX > 0) ) {

    deplacementX = abs(deplacementX) + 1;

    if (deplacementY < 0) deplacementY = deplacementY - 1;
    if (deplacementY > 0) deplacementY = deplacementY + 1;

    rectangleL = rectangleL - 5;
    score++;


  }





  if (circleX > width) deplacementX = -deplacementX;
  if (circleX < 0) {

    deplacementX = 0;
    deplacementY = 0;
  }

  if (circleY > height) deplacementY = -deplacementY;
  if (circleY < 0) deplacementY = abs(deplacementY);
}
