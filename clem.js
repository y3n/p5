function setup() {
  createCanvas(1920,1080);
  background(50);
}

var i = 0;

function draw() {
  colorMode(HSB, 400, 100, 100);
  stroke(i, 100, 100);
  strokeWeight(5);

  line(width*0.5, height*0.5, random(0, 1920), random(0, 1080));
  i++;

  if(i == 400){
    i = 1;
  }

}
