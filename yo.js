var paddingTop = 500
var screenWidth = 1920
var screenHeight = 1080

var verticalLinesCount = 100
var horizontalLinesCount = 50

function setup() {
  createCanvas(screenWidth, screenHeight, WEBGL)
  background(30)
}

function draw() {
  translate(100,100,-100);
  rotate(PI/4, [1,1,0]);
  box(300);
}
